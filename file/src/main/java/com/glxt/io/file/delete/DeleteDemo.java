package com.glxt.io.file.delete;

import java.io.File;

/**
 * file.delete()或file.deleteOnExit()方法只能删除文件或空文件夹
 * 
 * @author Administrator
 *
 */
public class DeleteDemo
{

	public static void main(String[] args)
	{
		// DeleteDemo.deleteDemo();
		DeleteDemo.deleteOnExitDemo();
	}

	/**
	 * boolean delete() 此方法删除表示此抽象路径名的文件或目录。
	 */
	public static void deleteDemo()
	{
		File file = null;
		boolean bool = false;

		try
		{
			// create new file
			file = new File("test.txt");

			// tries to delete a non-existing file
			bool = file.delete();

			// prints
			System.out.println("File deleted: " + bool);

			// creates file in the system
			file.createNewFile();

			// createNewFile() is invoked
			System.out.println("createNewFile() method is invoked");

			// tries to delete the newly created file
			bool = file.delete();

			// print
			System.out.println("File deleted: " + bool);

		} catch (Exception e)
		{
			// if any error occurs
			e.printStackTrace();
		}
	}

	/**
	 * void deleteOnExit() 此方法要求将表示此抽象路径名的文件或目录在虚拟机终止时被删除。
	 */
	public static void deleteOnExitDemo()
	{
		File file = null;

		try
		{
			// creates temporary file
			file = File.createTempFile("tmp", ".txt");

			// prints absolute path
			System.out.println("File path: " + file.getAbsolutePath());

			// deletes file when the virtual machine terminate
			file.deleteOnExit();

			// creates temporary file
			file = File.createTempFile("tmp", null);

			// prints absolute path
			System.out.print("File path: " + file.getAbsolutePath());

			// deletes file when the virtual machine terminate
			file.deleteOnExit();
		} catch (Exception e)
		{
			// if any error occurs
			e.printStackTrace();
		}
	}
}
