package com.glxt.io.file.creater;

import java.io.File;
import java.io.IOException;

public class CreateFile
{
	private File	file	= null;

	public File getFileHandler(String pathname)
	{
		file = new File(pathname);
		return file;
	}

	/**
	 * 创建文件
	 * 
	 * @param pathname
	 */
	public void createFile(String pathname)
	{
		file = new File(pathname);
		try
		{
			file.createNewFile();
		} catch (IOException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * 创建文件目录
	 * 
	 * @param pathname
	 * @return
	 */
	public boolean createDir(String pathname)
	{
		file = new File(pathname);
		boolean result = file.mkdir();
		return result;
	}

	/**
	 * 显示文件目录名
	 * 
	 * @param pathname
	 */
	public void listFilesName(String pathname)
	{
		file = new File(pathname);
		String[] names = file.list();
		for (String name : names)
		{
			System.out.println(name);
		}
	}

	public static void main(String[] args)
	{

		CreateFile cFile = new CreateFile();
		// cFile.createFile("d:\\test.txt");

		cFile.listFilesName("d://Program Files");

	}
}
