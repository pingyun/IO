package com.glxt.io.file.list;

import java.io.File;

public class ListFilesDemo
{
	public static void main(String[] args)
	{
		ListFilesDemo.listDemo();
		ListFilesDemo.listFilesDemo();
	}

	/**
	 * String[] list() 此方法返回的字符串命名表示此抽象路径名的目录中的文件和目录的数组。
	 */
	public static void listDemo()
	{
		File f = null;
		String[] paths;

		try
		{
			// create new file
			f = new File("d:/Program Files (x86)");

			// array of files and directory
			paths = f.list();

			// for each name in the path array
			for (String path : paths)
			{
				// prints filename and directory name
				System.out.println(path);
			}
		} catch (Exception e)
		{
			// if any error occurs
			e.printStackTrace();
		}
	}

	/**
	 * File[] listFiles() 此方法返回抽象路径名表示在表示此抽象路径名的目录中的文件的数组。
	 */
	public static void listFilesDemo()
	{
		File f = null;
		File[] paths;

		try
		{
			// create new file
			f = new File("d:/Program Files (x86)");

			// returns pathnames for files and directory
			paths = f.listFiles();

			// for each pathname in pathname array
			for (File path : paths)
			{
				// prints file and directory paths
				System.out.println(path);
			}
		} catch (Exception e)
		{
			// if any error occurs
			e.printStackTrace();
		}
	}
}
