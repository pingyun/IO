package com.glxt.io.file.creater;

import java.io.File;
import java.io.IOException;

import javax.xml.transform.Templates;

public class CreateTempFileDemo
{
	public static void main(String[] args)
	{
		// CreateTempFileDemo.twoParamdemo();

		CreateTempFileDemo.threeParamDemo();
	}

	/**
	 * static File createTempFile(String prefix, String suffix)
	 * 此方法创建的默认临时文件目录的空文件，使用给定前缀和后缀生成其名称。
	 */
	public static void twoParamdemo()
	{
		File file = null;

		try
		{
			// creates temporary file
			file = File.createTempFile("temp", ".txt");

			// prints absolute path
			System.out.println("File path: " + file.getAbsolutePath());

			// deletes file when the virtual machine terminate
			file.deleteOnExit();

			// creates temporary file
			file = File.createTempFile("tmp", null);

			// prints absolute path
			System.out.println("File path: " + file.getAbsolutePath());

			// deletes file when the virtual machine terminate
			file.deleteOnExit();
		} catch (IOException e)
		{
			// // if any error occurs
			e.printStackTrace();
		}
	}

	/**
	 * static File createTempFile(String prefix, String suffix, File directory)
	 * 此方法会在指定的目录中一个新的空文件，使用给定前缀和后缀字符串生成其名称。
	 */
	public static void threeParamDemo()
	{
		File file = null;

		try
		{
			// creates temporary file
			file = File.createTempFile("tmp", ".txt", new File("D:/"));

			// prints absolute path
			System.out.println("File path: " + file.getAbsolutePath());

			// deletes file when the virtual machine terminate
			file.deleteOnExit();

			// creates temporary file
			file = File.createTempFile("tmp", null, new File("D:/"));

			// prints absolute path
			System.out.println("File path: " + file.getAbsolutePath());

			// deletes file when the virtual machine terminate
			file.deleteOnExit();

		} catch (IOException e)
		{
			// // if any error occurs
			e.printStackTrace();
		}
	}
}
