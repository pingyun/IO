package com.glxt.io.file.creater;

import java.io.File;

public class MkdirDemo
{
	public static void main(String[] args)
	{
		MkdirDemo.mkdirDemo();
		MkdirDemo.mkdirsDemo();
	}

	/**
	 * boolean mkdir() 此方法创建此抽象路径名的目录。
	 */
	public static void mkdirDemo()
	{
		File f = null;
		boolean bool = false;

		try
		{
			// returns pathnames for files and directory
			f = new File("D:/Texts");
			// create
			bool = f.mkdir();

			// print
			System.out.println("Directory created? " + bool);
			f.delete();

		} catch (Exception e)
		{
			// if any error occurs
			e.printStackTrace();
		}
	}

	/**
	 * boolean mkdirs() 此方法创建此抽象路径名的目录，包括任何必需但不存在的父目录
	 */
	public static void mkdirsDemo()
	{

		File f = null;
		boolean bool = false;

		try
		{
			// returns pathnames for files and directory
			f = new File("D:/Texts/LNP/Java");

			// create directories
			bool = f.mkdirs();

			// print
			System.out.println("Directory created? " + bool);

		} catch (Exception e)
		{
			// if any error occurs
			e.printStackTrace();
		}
	}
}
