package com.glxt.io.file.getfile;

import java.io.File;

public class GetFileDemo
{
	public static void main(String[] args)
	{
		GetFileDemo.getAbsoluteFileDemo();
		GetFileDemo.getCanonicalFileDemo();
	}

	/**
	 * File getAbsoluteFile() 此方法返回此抽象路径名的绝对形式。
	 */
	public static void getAbsoluteFileDemo()
	{
		File f = null;
		File f1 = null;
		String path = "";
		boolean bool = false;

		try
		{
			// create new files
			f = new File("test.txt");

			// create new file in the system
			f.createNewFile();

			// create new file object from the absolute path
			f1 = f.getAbsoluteFile();

			// returns true if the file exists
			bool = f1.exists();

			// returns absolute pathname
			path = f1.getAbsolutePath();

			// if file exists
			if (bool)
			{
				// prints
				System.out.println(path + " Exists? " + bool);
			}
			f.delete();

		} catch (Exception e)
		{
			// if any error occurs
			e.printStackTrace();
		}
	}

	/**
	 * File getCanonicalFile() 此方法返回此抽象路径名的规范形式。
	 */
	public static void getCanonicalFileDemo()
	{
		File f = null;
		File f1 = null;
		String path = "";
		boolean bool = false;

		try
		{
			// create new files
			f = new File("test.txt");
			f.createNewFile();

			// create new canonical form file object
			f1 = f.getCanonicalFile();

			// returns true if the file exists
			bool = f1.exists();

			// returns absolute pathname
			path = f1.getAbsolutePath();

			// if file exists
			if (bool)
			{
				// prints
				System.out.println(path + " Exists? " + bool);
			}
			f.delete();
		} catch (Exception e)
		{
			// if any error occurs
			e.printStackTrace();
		}
	}
	
	
}
