package com.glxt.io.file.getpath;

import java.io.File;

public class GetPathDemo
{
	public static void main(String[] args)
	{
		GetPathDemo.getAbsolutePathDemo();
		GetPathDemo.getCanonicalPathDemo();
		GetPathDemo.getPathDemo();
		GetPathDemo.getParentDemo();
	}

	/**
	 * String getAbsolutePath() 此方法返回此抽象路径名的绝对路径名字符串。
	 */
	public static void getAbsolutePathDemo()
	{
		File f = null;
		String path = "";
		boolean bool = false;

		try
		{
			// create new files
			f = new File("test.txt");
			f.createNewFile();
			// returns true if the file exists
			bool = f.exists();

			// if file exists
			if (bool)
			{
				// get absolute path
				path = f.getAbsolutePath();

				// prints
				System.out.println("Absolute Pathname " + path);
			}
			f.delete();
		} catch (Exception e)
		{
			// if any error occurs
			e.printStackTrace();
		}
	}

	/**
	 * String getCanonicalPath() 此方法返回此抽象路径名的规范路径名字符串。
	 */
	public static void getCanonicalPathDemo()
	{
		File f = null;
		String path = "";
		boolean bool = false;

		try
		{
			// create new files
			f = new File("test.txt");
			f.createNewFile();
			// returns true if the file exists
			bool = f.exists();

			// if file exists
			if (bool)
			{
				// get absolute path
				path = f.getCanonicalPath();

				// prints
				System.out.println("Canonical Pathname " + path);
			}
			f.delete();
		} catch (Exception e)
		{
			// if any error occurs
			e.printStackTrace();
		}
	}

	/**
	 * String getPath() 此方法此抽象路径名转换为一个路径名字符串。
	 */
	public static void getPathDemo()
	{
		File f = null;
		String v;
		boolean bool = false;

		try
		{
			// create new file
			f = new File("D:/test.txt");
			f.createNewFile();
			// pathname string from abstract pathname
			v = f.getPath();

			// true if the file path exists
			bool = f.exists();

			// if file exists
			if (bool)
			{
				// prints
				System.out.println("pathname: " + v);
			}
			f.delete();
		} catch (Exception e)
		{
			// if any error occurs
			e.printStackTrace();
		}
	}

	/**
	 * String getParent() 此方法返回此抽象路径名的父路径名的字符串，或者null，如果此路径名没有指定父目录。
	 */
	public static void getParentDemo()
	{
		File f = null;
		String v;
		boolean bool = false;

		try
		{
			// create new file
			f = new File("test.txt");
			f.createNewFile();
			// get file name or directory name
			v = f.getParent();

			// true if the file path exists
			bool = f.exists();

			// if file exists
			if (bool)
			{
				// prints
				System.out.println("parent name: " + v);
			}
			f.delete();
		} catch (Exception e)
		{
			// if any error occurs
			e.printStackTrace();
		}
	}
}
