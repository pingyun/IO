package com.glxt.io.file;

import static org.junit.Assert.*;

import java.io.File;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.glxt.io.file.creater.CreateFile;

public class CreateFileTest
{
	CreateFile cFile;
	@Before
	public void setUp()
	{
		cFile = new CreateFile();
	}
	@After
	public void tearDown()
	{
		
	}
	@Test
	public void testGetFileHandler()
	{
		File file = cFile.getFileHandler("E:\\demoProject\\myeclipse\\javase\\IO\\file\\test.txt");
		assertNotNull(file);
	}

	@Test
	public void testCreateDir()
	{
		boolean result = cFile.createDir("d:\\test");
		assertTrue(result);
	}
	
	
}
